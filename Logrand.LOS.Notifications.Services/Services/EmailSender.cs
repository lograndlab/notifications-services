﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;

namespace Logrand.LOS.Notifications.Services.Services
{
    public class EmailSender
    {
        private string SMTP;
        private int Port;
        private string UserName;
        private string Password;

        public int NotificationID { get; set; }
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public List<string> Bcc { get; set; }
        public string From { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public List<Attachment> Attachments { get; set; }

        private Notifications.Models.NotificationsEntities db = new Notifications.Models.NotificationsEntities();

        public EmailSender()
        {
            this.SMTP = WebConfigurationManager.AppSettings["SMPT_SERVER"].ToString();
            this.Port = int.Parse(WebConfigurationManager.AppSettings["SMPT_PORT"].ToString());
            this.UserName = WebConfigurationManager.AppSettings["SMPT_USERNAME"].ToString();
            this.Password = WebConfigurationManager.AppSettings["SMPT_PASSWORD"].ToString();
        }
        
        public bool SendHTML()
        {
            try
            {
                this.From = "osi.notifications@logrand.com";

                SmtpClient SmtpServer = new SmtpClient(this.SMTP);
                var mail = new MailMessage();
                mail.From = new MailAddress(this.From);

                foreach (string email in this.To)
                {
                    mail.To.Add(new MailAddress(email));
                }

                mail.Subject = this.Subject;
                mail.IsBodyHtml = true;
                mail.Body = this.Body;

                SmtpServer.Port = this.Port;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(this.UserName, this.Password);
                SmtpServer.EnableSsl = false;
                SmtpServer.Send(mail);
                //Entry save
                this.save(true);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            
        }

        public bool Send()
        {
            try
            {
                this.From = "osi.notifications@logrand.com";

                SmtpClient SmtpServer = new SmtpClient(this.SMTP);
                var mail = new MailMessage();
                mail.From = new MailAddress(this.From);

                foreach (string email in this.To)
                {
                    mail.To.Add(new MailAddress(email));
                }

                mail.Subject = this.Subject;
                mail.IsBodyHtml = true;
                mail.Body = this.Body;

                SmtpServer.Port = this.Port;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(this.UserName, this.Password);
                SmtpServer.EnableSsl = false;
                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool SendHTMLAsync()
        {
            try
            {
                this.From = "osi.notifications@logrand.com";

                SmtpClient SmtpServer = new SmtpClient(this.SMTP);
                var mail = new MailMessage();
                mail.From = new MailAddress(this.From);

                foreach (string email in this.To)
                {
                    mail.To.Add(new MailAddress(email));
                }

                mail.Subject = this.Subject;
                mail.IsBodyHtml = true;
                mail.Body = this.Body;

                SmtpServer.Port = this.Port;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(this.UserName, this.Password);
                SmtpServer.EnableSsl = false;
                SmtpServer.Send(mail);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool SendAsync()
        {
            try
            {
                this.From = "osi.notifications@logrand.com";

                SmtpClient SmtpServer = new SmtpClient(this.SMTP);
                var mail = new MailMessage();
                mail.From = new MailAddress(this.From);

                foreach (string email in this.To)
                {
                    mail.To.Add(new MailAddress(email));
                }

                mail.Subject = this.Subject;
                mail.IsBodyHtml = true;
                mail.Body = this.Body;

                SmtpServer.Port = this.Port;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(this.UserName, this.Password);
                SmtpServer.EnableSsl = false;
                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private void save(bool? isHTML)
        {
            try
            {
                Notifications.Models.Email model = new Notifications.Models.Email();

                model.NotificationID = this.NotificationID;
                model.From = this.From;

                if (this.To != null && this.To.Count > 0)
                    model.To = string.Join(";", this.To);

                if (this.Cc != null && this.Cc.Count > 0)
                    model.CC = string.Join(";", this.Cc);

                if (this.Bcc != null && this.Bcc.Count > 0)
                    model.BCC = string.Join(";", this.Bcc);

                model.Subject = this.Subject;
                model.Body = "";
                model.IsHTML = isHTML;
                model.CreatedDate = DateTime.Now;

                db.Email.Add(model);
                db.SaveChanges();
            }
            catch (Exception e)
            {

            }
        }
    }



    public class Attachment
    {
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public MemoryStream Stream { get; set; }
    }
}