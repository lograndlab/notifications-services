﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Notifications.Services.Models
{
    public class OrdenCompra
    {
        public int EventoID { get; set; }
        public string Numero { get; set; }
        public string Fecha { get; set; }
        public string Solicitante { get; set; }
        public decimal Total { get; set; }
        public string Moneda { get; set; }
        public string CentroLogistico { get; set; }
        public Subscriber Suscriptores { get; set; }
        public List<OrdenCompraDetails> Details { get; set; }
        public List<AuthorizationTracker> FlujoAutorizadores { get; set; }

        public static OrdenCompra get(OrdenCompra ordenCompra)
        {
            ordenCompra.Details = OrdenCompraDetails.get(ordenCompra.Numero);
            return ordenCompra;
        }
    }
}