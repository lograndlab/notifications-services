﻿using Logrand.LOS.Notifications.Services.Services;
using Logrand.LOS.Notifications.Services.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Notifications.Services.Models
{
    public class SAPSystem
    {
        public Solped Solped { get; set; }
        public OrdenCompra OrdenCompra { get; set; }

        private static Notifications.Models.NotificationsEntities db = new Notifications.Models.NotificationsEntities();

        public static void sendNotification(SAPSystem request)
        {
            if (request.Solped != null)
            {
                solpedNotification(request.Solped);
            }

            if(request.OrdenCompra != null)
            {
                ordenCompraNotification(request.OrdenCompra);
            }
        }

        private static void solpedNotification(Solped Solped)
        {
            //Valida solped
            solpedValidate(Solped);

            //Leer evento de SAP
            var evento = db.NOT_GET_EVENT_BY_ID(Solped.EventoID).FirstOrDefault();

            if(evento == null)
            {
                throw new Exception("El evento de la Solped no existe.");
            }

            var emailTemplate = evento.EmailTemplateID != null ? db.NOT_GET_EMAIL_TEMPLATE_BY_ID(evento.EmailTemplateID).FirstOrDefault() : null;

            //Obtinene la lista de usuarios SAP y grupos de compra
            List<string> usuariosSAP = Solped.Suscriptores.UsuariosSAP == null ? new List<string>() : Solped.Suscriptores.UsuariosSAP;
            List<string> gruposCompra = Solped.Suscriptores.GruposCompras == null ? new List<string>() : Solped.Suscriptores.GruposCompras;

            List<string> To = new List<string>();
            //Buscar usuarios SAP y agregarlos
            foreach (string user in usuariosSAP)
            {
                var result = db.NOT_GET_EMAIL_BY_USERSAP(user).FirstOrDefault();
                To.Add(result.Email);
            }
            //Buscar grupos de compra y agregarlos
            foreach (string grupo in gruposCompra)
            {
                int numero = int.Parse(grupo);
                var result = db.NOT_GET_EMAIL_BY_NUMBER_GRUPO_COMPRA(numero).FirstOrDefault();
                To.Add(result.Email);
            }

            //Crear notificacion
            Notifications.Models.Notification notification = new Notifications.Models.Notification();

            notification.ApplicationID = 999; //Find application id
            notification.CreatedDate = DateTime.Now;

            db.Notification.Add(notification);
            db.SaveChanges();

            EmailSender email = new EmailSender();
            email.NotificationID = notification.ID;
            email.To = To;
            email.Subject = evento.Name;
            email.Body = Utils.Template.SolpedTemplate.createEmailTemplate(Solped.get(Solped), emailTemplate.PathBodyHtml);
            email.SendHTML();
        }

        private static void ordenCompraNotification(OrdenCompra OrdenCompra)
        {
            //Valida orden de compra
            ordenCompraValidate(OrdenCompra);

            //Leer evento de SAP
            var evento = db.NOT_GET_EVENT_BY_ID(OrdenCompra.EventoID).FirstOrDefault();

            if (evento == null)
            {
                throw new Exception("El evento de la Orden de Compra no existe.");
            }

            var emailTemplate = evento.EmailTemplateID != null ? db.NOT_GET_EMAIL_TEMPLATE_BY_ID(evento.EmailTemplateID).FirstOrDefault() : null;

            //Obtinene la lista de usuarios SAP y grupos de compra
            List<string> usuariosSAP = OrdenCompra.Suscriptores.UsuariosSAP == null ? new List<string>() : OrdenCompra.Suscriptores.UsuariosSAP;
            List<string> gruposCompra = OrdenCompra.Suscriptores.GruposCompras == null ? new List<string>() : OrdenCompra.Suscriptores.GruposCompras;

            List<string> To = new List<string>();
            //Buscar usuarios SAP y agregarlos
            foreach (string user in usuariosSAP)
            {
                var result = db.NOT_GET_EMAIL_BY_USERSAP(user).FirstOrDefault();
                To.Add(result.Email);
            }
            //Buscar grupos de compra y agregarlos
            foreach (string grupo in gruposCompra)
            {
                int numero = int.Parse(grupo);
                var result = db.NOT_GET_EMAIL_BY_NUMBER_GRUPO_COMPRA(numero).FirstOrDefault();
                To.Add(result.Email);
            }

            //Crear notificacion
            Notifications.Models.Notification notification = new Notifications.Models.Notification();

            notification.ApplicationID = 999; //Find application id
            notification.CreatedDate = DateTime.Now;

            db.Notification.Add(notification);
            db.SaveChanges();

            EmailSender email = new EmailSender();
            email.NotificationID = notification.ID;
            email.To = To;
            email.Subject = evento.Name;
            email.Body = Utils.Template.OrdenCompraTemplate.createEmailTemplate(OrdenCompra.get(OrdenCompra), emailTemplate.PathBodyHtml);
            email.SendHTML();
        }

        private static void solpedValidate(Solped Solped)
        {
            List<string> usuariosSAP = Solped.Suscriptores.UsuariosSAP;
            List<string> gruposCompra = Solped.Suscriptores.GruposCompras;

            if (usuariosSAP.Count <= 0 && gruposCompra.Count <= 0)
            {
                throw new Exception("La notificación de Solped no cuenta con usuarios SAP o grupos de compra.");
            }

            if (Solped.EventoID == 0)
            {
                throw new Exception("La notificación de Solped no cuenta con un EventoID.");
            }

            if (Solped.Numero == string.Empty)
            {
                throw new Exception("La notificación de Solped no cuenta con un Numero.");
            }

            if (Solped.Fecha == string.Empty)
            {
                throw new Exception("La notificación de Solped no cuenta con una Fecha.");
            }

            if (Solped.Solicitante == string.Empty)
            {
                throw new Exception("La notificación de Solped no cuenta con un Solicitante.");
            }
        }

        private static void ordenCompraValidate(OrdenCompra OrdenCompra)
        {
            List<string> usuariosSAP = OrdenCompra.Suscriptores.UsuariosSAP;
            List<string> gruposCompra = OrdenCompra.Suscriptores.GruposCompras;

            if (usuariosSAP.Count <= 0 && gruposCompra.Count <= 0)
            {
                throw new Exception("La notificación de Orden de Compra no cuenta con usuarios SAP o grupos de compra.");
            }

            if (OrdenCompra.EventoID == 0)
            {
                throw new Exception("La notificación de Orden de Compra no cuenta con un EventoID.");
            }

            if (OrdenCompra.Numero == string.Empty)
            {
                throw new Exception("La notificación de Orden de Compra no cuenta con un Numero.");
            }

            if (OrdenCompra.Fecha == string.Empty)
            {
                throw new Exception("La notificación de Orden de Compra no cuenta con una Fecha.");
            }

            if (OrdenCompra.Solicitante == string.Empty)
            {
                throw new Exception("La notificación de Orden de Compra no cuenta con un Solicitante.");
            }
        }
    }
}