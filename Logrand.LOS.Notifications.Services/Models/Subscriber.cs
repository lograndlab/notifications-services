﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Notifications.Services.Models
{
    public class Subscriber
    {
        public List<string> UsuariosSAP { get; set; }
        public List<string> GruposCompras { get; set; }
        public List<string> Providers { get; set; }
    }
}