﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Notifications.Services.Models
{
    public class SolpedDetails
    {
        public string preq_no { get; set; }
        public int preq_item { get; set; }
        public string preq_name { get; set; }
        public string rel_status { get; set; }
        public string material { get; set; }
        public decimal quantity { get; set; }
        public string short_text { get; set; }
        public string cost_ctr { get; set; }
        public string unit { get; set; }
        public string deliv_date { get; set; }
        public string agreement { get; set; }
        public string des_vendor { get; set; }
        public string knttx { get; set; }
        public string wgbez60 { get; set; }
        public string purch_org { get; set; }
        public decimal c_amt_bapi { get; set; }
        public string currency { get; set; }
        public string texto_pos { get; set; }
        public string texto_nota { get; set; }
        public decimal total { get; set; }
        public string delete_ind { get; set; }
        public string req_blocked { get; set; }

        public static List<SolpedDetails> get(string ID)
        {
            var client = new RestClient("http://172.16.2.30/sap.api/api/solpeds/Partida/" + ID);
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            if (response.StatusDescription == "OK")
            {
                return JsonConvert.DeserializeObject<List<SolpedDetails>>(response.Content);
            }

            return null;
        }
    }
}