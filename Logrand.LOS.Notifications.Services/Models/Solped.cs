﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Notifications.Services.Models
{
    public class Solped
    {
        public int EventoID { get; set; }
        public string Numero { get; set; }
        public string Fecha { get; set; }
        public string Solicitante { get; set; }
        public decimal Total { get; set; }
        public string Moneda { get; set; }
        public string CentroLogistico { get; set; }
        public Subscriber Suscriptores { get; set; }
        public List<SolpedDetails> Details { get; set; }
        public List<AuthorizationTracker> FlujoAutorizadores { get; set; }

        public static Solped get(Solped solped)
        {
            solped.Details = SolpedDetails.get(solped.Numero);
            return solped;
        }
    }
}