﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Notifications.Services.Models
{
    public class AuthorizationTracker
    {
        public string Autorizador { get; set; }
        public int NivelAutorizacion { get; set; }
        public string FechaAutorizacion { get; set; }
    }
}