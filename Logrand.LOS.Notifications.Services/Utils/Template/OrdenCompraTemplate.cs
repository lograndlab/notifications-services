﻿using Logrand.LOS.Notifications.Services.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Notifications.Services.Utils.Template
{
    public class OrdenCompraTemplate
    {
        /*
         * @Params
         * {body_title}
         * {body_text1}
         * {table_head}
         * {table_body}
         * {table_head_auth}
         * {table_body_auth}
         */
        public static string createEmailTemplate(OrdenCompra ordenCompra, string pathTemplate)
        {
            string template = "";
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath(pathTemplate)))
            {
                template = reader.ReadToEnd();
                reader.Close();
            }

            //Header create
            template = template.Replace("{solped_id}", ordenCompra.Numero);
            template = template.Replace("{solicitante}", ordenCompra.Solicitante);
            template = template.Replace("{centro_logistico}", ordenCompra.CentroLogistico);
            template = template.Replace("{fecha}", ordenCompra.Fecha);

            //Body create
            string tableHead = "<tr>";
            tableHead += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>POS</strong></th>";
            tableHead += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>CANT</strong></th>";
            tableHead += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>UM</strong></th>";
            tableHead += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>TEXTO</strong></th>";
            tableHead += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>MONTO</strong></th>";
            tableHead += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>TOTAL</strong></th>";
            tableHead += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>MONEDA</strong></th>";
            tableHead += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>SOLICITANTE</strong></th>";
            tableHead += "</tr>";

            template = template.Replace("{table_head}", tableHead);

            string tableBody = "";
            foreach (OrdenCompraDetails detail in ordenCompra.Details)
            {
                tableBody += "<tr>";
                tableBody += "<td style=\"padding-left: 2px; padding-right: 2px;\">" + detail.preq_item + "</td>";
                tableBody += "<td style=\"padding-left: 2px; padding-right: 2px;\">" + detail.quantity.ToString("#,###.##") + "</td>";
                tableBody += "<td style=\"padding-left: 2px; padding-right: 2px;\">" + detail.unit + "</td>";
                tableBody += "<td>" + detail.short_text + "</td>";
                tableBody += "<td style=\"padding-left: 2px; padding-right: 2px;\">$" + detail.c_amt_bapi.ToString("###,###,###.##") + "</td>";
                tableBody += "<td style=\"padding-left: 2px; padding-right: 2px;\">$" + detail.total.ToString("###,###,###.##") + "</td>";
                tableBody += "<td style=\"padding-left: 2px; padding-right: 2px;\">" + detail.currency + "</td>";
                tableBody += "<td style=\"padding-left: 2px; padding-right: 2px;\">" + detail.preq_name + "</td>";
                tableBody += "</tr>";
            }
            template = template.Replace("{table_body}", tableBody);
            template = template.Replace("{total}", "$" + ordenCompra.Total.ToString("###,###,###.##") + " " + ordenCompra.Moneda);

            string tableHeadAuth = "<tr>";
            tableHeadAuth += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>Nombre</strong></th>";
            tableHeadAuth += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>Fecha</strong></th>";
            tableHeadAuth += "<th style=\"text-align:center; padding-left: 2px; padding-right: 2px;\"><strong>Estatus</strong></th>";
            tableHeadAuth += "</tr>";

            template = template.Replace("{table_head_auth}", tableHeadAuth);

            string tableBodyAuth = "";
            foreach (AuthorizationTracker autorizador in ordenCompra.FlujoAutorizadores.OrderBy(x => x.NivelAutorizacion))
            {
                tableBodyAuth += "<tr>";
                tableBodyAuth += "<td style=\"padding-left: 2px; padding-right: 2px;\">" + autorizador.Autorizador + "</td>";
                tableBodyAuth += "<td style=\"padding-left: 2px; padding-right: 2px;\">" + autorizador.FechaAutorizacion + "</td>";

                if (autorizador.FechaAutorizacion == null)
                {
                    tableBodyAuth += "<td style=\"padding-left: 2px; padding-right: 2px;\"></td>";
                }
                else
                {
                    tableBodyAuth += "<td style=\"padding-left: 2px; padding-right: 2px;\">Aprobado</td>";
                }

                tableBodyAuth += "</tr>";
            }

            template = template.Replace("{table_body_auth}", tableBodyAuth);

            return template;
        }
    }
}