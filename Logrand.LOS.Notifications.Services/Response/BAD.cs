﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Logrand.LOS.Notifications.Services.Response
{
    public class BAD
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public string TechnicalMessage { get; set; }
    }
}