﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Logrand.LOS.Notifications.Services.Response
{
    public class OK
    {
        public string Status { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}