﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Logrand.LOS.Notifications.Services.Controllers
{
    [RoutePrefix("api/users/sap")]
    public class UsersSAPController : ApiController
    {
        private Notifications.Models.NotificationsEntities db = new Notifications.Models.NotificationsEntities();

        [HttpGet]
        [Route("")]
        public HttpResponseMessage getUsersSAP()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, db.NOT_GET_USERS_SAP().ToList());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response.BAD()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Message = "No se pudo cargar la lista de Usuarios SAP",
                    TechnicalMessage = e.Message
                });
            }
        }

        [HttpGet]
        [Route("{id}")]
        public HttpResponseMessage getUserSAP(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, db.NOT_GET_USERS_SAP_BY_ID(id).FirstOrDefault());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response.BAD()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Message = "No se encontro el usuario de SAP.",
                    TechnicalMessage = e.Message
                });
            }
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage createUserSAP(Models.UserSAP request)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, db.NOT_ADD_USER_SAP(request.UserSap, request.Email, request.CreatedBy, request.CreatedDate, request.Active).FirstOrDefault());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response.BAD()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Message = "No se pudo agregar un UserSAP",
                    TechnicalMessage = e.Message
                });
            }
        }

        [HttpPut]
        [Route("")]
        public HttpResponseMessage updateUserSAP(Models.UserSAP request)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, db.NOT_UPDATE_USER_SAP(request.ID, request.UserSap, request.Email, request.CreatedBy, request.CreatedDate, request.Active).FirstOrDefault());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response.BAD()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Message = "No se pudo actualizar un UserSAP",
                    TechnicalMessage = e.Message
                });
            }
        }
    }
}
