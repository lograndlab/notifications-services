﻿using Logrand.LOS.Notifications.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Logrand.LOS.Notifications.Services.Controllers
{
    [RoutePrefix("api/sap")]
    public class SAPController : ApiController
    {
        [HttpPost]
        [Route("")]
        public HttpResponseMessage notificationsSap(SAPSystem sapSystem)
        {
            try
            {
                SAPSystem.sendNotification(sapSystem);

                return Request.CreateResponse(HttpStatusCode.OK, new Response.OK()
                {
                    Status = "OK",
                    StatusCode = HttpStatusCode.OK
                });
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadGateway, new Response.BAD()
                {
                    Message = "No se pudo enviar la notificación.",
                    TechnicalMessage = e.Message,
                    StatusCode = HttpStatusCode.InternalServerError
                });
            }
            
        }
    }
}
