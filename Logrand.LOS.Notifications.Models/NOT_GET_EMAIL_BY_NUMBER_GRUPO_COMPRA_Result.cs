//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Logrand.LOS.Notifications.Models
{
    using System;
    
    public partial class NOT_GET_EMAIL_BY_NUMBER_GRUPO_COMPRA_Result
    {
        public int ID { get; set; }
        public Nullable<int> Number { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
